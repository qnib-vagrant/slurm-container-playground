servers=[
  {
    :hostname => "compute1",
    :ip => "192.168.56.11",
    :autostart => true
  },{
    :hostname => "compute2",
    :ip => "192.168.56.12",
    :autostart => true
  },{
    :hostname => "compute3",
    :ip => "192.168.56.13",
    :autostart => false
  },{
    :hostname => "compute4",
    :ip => "192.168.56.14",
    :autostart => false
  }
]

$script = <<SCRIPT
   cat >> /etc/hosts << EOF
192.168.56.10 headnode
192.168.56.11 compute1
192.168.56.12 compute2
192.168.56.13 compute3
192.168.56.14 compute4
EOF
  chown alice: /nfs/share/spack/cache
  if [[ "$(hostname)" == "headnode" ]];then
    echo ">> Copy munge.key to NFS share"
	  cp -rf /etc/munge/munge.key /nfs/share/
    chmod 444 /nfs/share/munge.key
    if [ ! -f ~alice/.ssh/id_rsa.pub ];then
      su - alice -c '< /dev/zero ssh-keygen -q -N ""'
      su - alice -c 'cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys'
      echo ""
    fi
    ### Spack hotfix
    if [[ ! -d /nfs/share/spack/pkg/.spack-db ]];then
      mkdir -p /nfs/share/spack/pkg/.spack-db
    fi
    chown -R alice: /nfs/share/spack/pkg/.spack-db
  else 
    echo ">> Wait for /nfs/share/munge.key"
    while [ ! -f /nfs/share/munge.key ]; do 
      echo -n "."
      sleep 1; 
    done
    echo " OK"
    echo ">> Copy munge.key from NFS share"    
	  cp -rf /nfs/share/munge.key /etc/munge/munge.key
    chmod 400 /etc/munge/munge.key
  fi
  for svc in munge $@;do
    sleep 2
    echo ">> Restart ${svc}"
    systemctl restart ${svc}
  done
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.define "headnode", primary: true do |head|
    head.vm.box = "qnib/rocky8-slurmctld"
    head.vm.box_version = "2022.03.16"
    head.vbguest.auto_update = false
    head.vm.network "private_network", ip: "192.168.56.10"
    head.vm.hostname = "headnode"
    head.ssh.username = "alice"
    head.vm.provider "virtualbox" do |vb|
      vb.memory = 3048
      vb.cpus = 4
    end
    head.vm.provision "shell" do |s|
      s.inline = $script
      s.args   = ["slurmctld", "slurmd"]
    end
  end
  servers.each do |machine|
    config.vm.define machine[:hostname], autostart:machine[:autostart] do |compute|
      compute.vm.box = "qnib/rocky8-slurmd"
      compute.vm.box_version = "2022.03.16"
      compute.vbguest.auto_update = false
      compute.vm.hostname = machine[:hostname]
      compute.vm.network "private_network", ip: machine[:ip]
      compute.ssh.username = "vagrant"
      compute.vm.provider "virtualbox" do |vb|
        vb.memory = 1024
        vb.cpus = 1
      end
      compute.vm.provision "shell" do |s|
        s.inline = $script
        s.args   = ["slurmd"]
      end
    end
  end
end
