# slurm-container-playground

HPC Cluster with Spack and container runtimes

### Startup

The stack comprises of one headnode (2CPU cores, 2GB RAM) and four compute nodes (1CPU/1GB). They are all light weight nodes so do not expect to run big workloads on them.
It's designed to run basic container commands and explore different runtimes.

```
vagrant up headnode
```


