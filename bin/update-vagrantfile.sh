#!/bin/bash

if [[ "${TRIGGER_PROJECT}" == "rocky8-slurmctld" ]];then
    VBOX_VERSION=$(vagrant cloud box show qnib/${TRIGGER_PROJECT}|awk '/Current Version/{print $3}')
    git checkout main
    git pull "http://gitlab-ci-token:${SSH_PRIVATE_KEY_TOOLKIT}@${CI_SERVER_HOST}/vagrant/${CI_PROJECT_NAME}.git" main
    sed -i -e "s/head.vm.box_version = .*/head.vm.box_version = \"${VBOX_VERSION}\"/" Vagrantfile
    git commit -am "Update Vagrantfile for ${TRIGGER_PROJECT} to use ${VBOX_VERSION}"
    git push "http://gitlab-ci-token:${SSH_PRIVATE_KEY_TOOLKIT}@${CI_SERVER_HOST}/vagrant/${CI_PROJECT_NAME}.git"
elif [[ "${TRIGGER_PROJECT}" == "rocky8-slurmd" ]];then
    VBOX_VERSION=$(vagrant cloud box show qnib/${TRIGGER_PROJECT}|awk '/Current Version/{print $3}')
    git checkout main
    git pull "http://gitlab-ci-token:${SSH_PRIVATE_KEY_TOOLKIT}@${CI_SERVER_HOST}/vagrant/${CI_PROJECT_NAME}.git" main
    git commit -am "Update Vagrantfile for ${TRIGGER_PROJECT} to use ${VBOX_VERSION}"
    sed -i -e "s/compute.vm.box_version = .*/compute.vm.box_version = \"${VBOX_VERSION}\"/" Vagrantfile
    git push "http://gitlab-ci-token:${SSH_PRIVATE_KEY_TOOLKIT}@${CI_SERVER_HOST}/vagrant/${CI_PROJECT_NAME}.git"
fi
    
